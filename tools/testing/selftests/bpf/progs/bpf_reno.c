// SPDX-License-Identifier: GPL-2.0-only

/* WARNING: This implemenation is not necessarily the same
 * as the tcp_reno.c.  The purpose is mainly for testing
 * and logging slow start behavior.
 *
 * 
 * Compile BPF Module:
 clang  -g -D__TARGET_ARCH_x86 -mlittle-endian -I../../../../tools/testing/selftests/bpf/tools/include -I../../../../tools/testing/selftests/bpf -I../../../../tools/include/uapi -I../../../../tools/testing/selftests/usr/include -idirafter /usr/local/include -idirafter /usr/lib/llvm-12/lib/clang/12.0.0/include -idirafter /usr/include/x86_64-linux-gnu -idirafter /usr/include  -Wno-compare-distinct-pointer-types -O2 -target bpf -c progs/bpf_reno.c -o bpf_reno.o -mcpu=v3
 * 
 * Compile Logging Module:
 * 
 id=$(sudo bpftool struct_ops show | cut -f 1 -d ':')
 sudo bpftool struct_ops unregister id ${id}
 sudo bpftool struct_ops register ./bpf_reno.o
 sudo sysctl -w net.ipv4.tcp_allowed_congestion_control="reno cubic bpf_reno"
 sudo sysctl -w net.ipv4.tcp_congestion_control="bpf_reno"
 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
./test_tcp_reno | tee cwnd.log
 * 
 */

#include <linux/bpf.h>
#include <linux/stddef.h>
#include <linux/tcp.h>
#include <linux/sock_diag.h>
#include "bpf_tcp_helpers.h"
#include "bpf/bpf_tracing.h"
#include <stddef.h>

#include <linux/types.h>
#include <linux/stddef.h>

#include <sys/socket.h>
#include <linux/bpf_common.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>
#include <linux/btf.h>
#include <linux/filter.h>

// in tools/lib/bpf/libbpf_internal.h
#ifndef likely
#define likely(x) __builtin_expect(!!(x), 1)
#endif

char _license[] SEC("license") = "GPL";

enum {
	TCP_ESTABLISHED = 1,
	TCP_SYN_SENT,
	TCP_SYN_RECV,
	TCP_FIN_WAIT1,
	TCP_FIN_WAIT2,
	TCP_TIME_WAIT,
	TCP_CLOSE,
	TCP_CLOSE_WAIT,
	TCP_LAST_ACK,
	TCP_LISTEN,
	TCP_CLOSING,	/* Now a valid state */
	TCP_NEW_SYN_RECV,

	TCP_MAX_STATES	/* Leave at the end! */
};

//ToDo CONFIG_HZ is used instead of HZ? Could it a problem?
extern unsigned CONFIG_HZ __kconfig;
#define USEC_PER_SEC	1000000ULL
static inline __u64 tcp_compute_delivery_rate(const struct tcp_sock *tp)
{
	__u32 rate = tp->rate_delivered;
	__u32 intv = tp->rate_interval_us;
	__u64 rate64 = 0;

	if (rate && intv) {
		rate64 = (__u64)rate * tp->mss_cache * USEC_PER_SEC;
		rate64 = rate64/intv;
	}
	return rate64;
}
unsigned int jiffies_to_usecs(const unsigned long j)
{
	return (USEC_PER_SEC / CONFIG_HZ) * j;
}

static inline __u32 minmax_get(const struct minmax *m)
{
	return m->s[0].v;
}

static inline __u32 tcp_min_rtt(const struct tcp_sock *tp)
{
	return minmax_get(&tp->rtt_min);
}

static inline __u32 minmax_reset(struct minmax *m, __u32 t, __u32 meas)
{
	struct minmax_sample val = { .t = t, .v = meas };

	m->s[2] = m->s[1] = m->s[0] = val;
	return m->s[0].v;
}

__u32 minmax_running_max(struct minmax *m, __u32 win, __u32 t, __u32 meas);
__u32 minmax_running_min(struct minmax *m, __u32 win, __u32 t, __u32 meas);

static inline int tcp_is_sack(const struct tcp_sock *tp)
{
	return likely(tp->rx_opt.sack_ok);
}

enum tcp_chrono {
	TCP_CHRONO_UNSPEC,
	TCP_CHRONO_BUSY, /* Actively sending data (non-empty write queue) */
	TCP_CHRONO_RWND_LIMITED, /* Stalled by insufficient receive window */
	TCP_CHRONO_SNDBUF_LIMITED, /* Stalled by insufficient send buffer */
	__TCP_CHRONO_MAX,
};

static void tcp_get_info_chrono_stats(const struct tcp_sock *tp,
				      struct tcp_info *info)
{
	__u64 stats[__TCP_CHRONO_MAX], total = 0;
	enum tcp_chrono i;

	for (i = TCP_CHRONO_BUSY; i < __TCP_CHRONO_MAX; ++i) {
		stats[i] = tp->chrono_stat[i - 1];
		if (i == tp->chrono_type)
			stats[i] += tcp_jiffies32 - tp->chrono_start;
		stats[i] *= USEC_PER_SEC / CONFIG_HZ;
		total += stats[i];
	}

	info->tcpi_busy_time = total;
	info->tcpi_rwnd_limited = stats[TCP_CHRONO_RWND_LIMITED];
	info->tcpi_sndbuf_limited = stats[TCP_CHRONO_SNDBUF_LIMITED];
}

/* Return information about state of tcp endpoint in API format. */
static inline void tcp_get_info(struct sock *sk, struct tcp_info *info)
{
	const struct tcp_sock *tp = tcp_sk(sk); /* iff sk_type == SOCK_STREAM */
	const struct inet_connection_sock *icsk = inet_csk(sk);
	unsigned long rate;
	__u32 now;
	__u64 rate64;

	info->tcpi_state = 0;//inet_sk_state_load(sk);

	/* Report meaningful fields for all TCP states, including listeners */
	rate = sk->sk_pacing_rate;
	rate64 = (rate != ~0UL) ? rate : ~0ULL;
	info->tcpi_pacing_rate = rate64;

	rate = sk->sk_max_pacing_rate;
	rate64 = (rate != ~0UL) ? rate : ~0ULL;
	info->tcpi_max_pacing_rate = rate64;

	info->tcpi_reordering = tp->reordering;
	info->tcpi_snd_cwnd = tp->snd_cwnd;

	if (info->tcpi_state == TCP_LISTEN) {
		/* listeners aliased fields :
		 * tcpi_unacked -> Number of children ready for accept()
		 * tcpi_sacked  -> max backlog
		 */
		info->tcpi_unacked = sk->sk_ack_backlog;
		info->tcpi_sacked = sk->sk_max_ack_backlog;
		return;
	}

	info->tcpi_ca_state = icsk->icsk_ca_state;
	info->tcpi_retransmits = icsk->icsk_retransmits;
	info->tcpi_probes = icsk->icsk_probes_out;
	info->tcpi_backoff = icsk->icsk_backoff;

	
	if (tp->rx_opt.tstamp_ok)
		info->tcpi_options |= TCPI_OPT_TIMESTAMPS;
	if (tcp_is_sack(tp))
		info->tcpi_options |= TCPI_OPT_SACK;
	if (tp->rx_opt.wscale_ok) {
		info->tcpi_options |= TCPI_OPT_WSCALE;
		info->tcpi_snd_wscale = tp->rx_opt.snd_wscale;
		info->tcpi_rcv_wscale = tp->rx_opt.rcv_wscale;
	}

	if (tp->ecn_flags & TCP_ECN_OK)
		info->tcpi_options |= TCPI_OPT_ECN;
	if (tp->ecn_flags & TCP_ECN_SEEN)
		info->tcpi_options |= TCPI_OPT_ECN_SEEN;
	if (tp->syn_data_acked)
		info->tcpi_options |= TCPI_OPT_SYN_DATA;
	
	info->tcpi_rto = jiffies_to_usecs(icsk->icsk_rto);
	info->tcpi_ato = jiffies_to_usecs(icsk->icsk_ack.ato);
	info->tcpi_snd_mss = tp->mss_cache;
	info->tcpi_rcv_mss = icsk->icsk_ack.rcv_mss;
	
	info->tcpi_unacked = tp->packets_out;
	info->tcpi_sacked = tp->sacked_out;

	info->tcpi_lost = tp->lost_out;
	info->tcpi_retrans = tp->retrans_out;

	now = tcp_jiffies32;
	info->tcpi_last_data_sent = jiffies_to_usecs(now - tp->lsndtime);
	info->tcpi_last_data_recv = jiffies_to_usecs(now - icsk->icsk_ack.lrcvtime);
	info->tcpi_last_ack_recv = jiffies_to_usecs(now - tp->rcv_tstamp);
	
	info->tcpi_pmtu = icsk->icsk_pmtu_cookie;
	info->tcpi_rcv_ssthresh = tp->rcv_ssthresh;
	info->tcpi_rtt = tp->srtt_us >> 3;
	info->tcpi_rttvar = tp->mdev_us >> 2;
	info->tcpi_snd_ssthresh = tp->snd_ssthresh;
	info->tcpi_advmss = tp->advmss;
	
	info->tcpi_rcv_rtt = tp->rcv_rtt_est.rtt_us >> 3;
	info->tcpi_rcv_space = tp->rcvq_space.space;

	info->tcpi_total_retrans = tp->total_retrans;
	
	info->tcpi_bytes_acked = tp->bytes_acked;
	info->tcpi_bytes_received = tp->bytes_received;
	if (tp->write_seq > tp->snd_nxt)
		info->tcpi_notsent_bytes = tp->write_seq - tp->snd_nxt;
	else
		info->tcpi_notsent_bytes = 0;
	tcp_get_info_chrono_stats(tp, info);

	info->tcpi_segs_out = tp->segs_out;
	info->tcpi_segs_in = tp->segs_in;
	
	info->tcpi_min_rtt = tcp_min_rtt(tp);
	
	info->tcpi_data_segs_in = tp->data_segs_in;
	info->tcpi_data_segs_out = tp->data_segs_out;
	
	
	info->tcpi_delivery_rate_app_limited = tp->rate_app_limited ? 1 : 0;
	rate64 = tcp_compute_delivery_rate(tp);
	if (rate64)
		info->tcpi_delivery_rate = rate64;
	info->tcpi_delivered = tp->delivered;
	info->tcpi_delivered_ce = tp->delivered_ce;
	info->tcpi_bytes_sent = tp->bytes_sent;
	info->tcpi_bytes_retrans = tp->bytes_retrans;
	info->tcpi_dsack_dups = tp->dsack_dups;
	info->tcpi_reord_seen = tp->reord_seen;
	info->tcpi_rcv_ooopack = tp->rcv_ooopack;
	
	info->tcpi_snd_cwnd = tp->snd_cwnd;
	info->tcpi_fastopen_client_fail = tp->fastopen_client_fail;
	
	return;
}

struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 256*4096);
	__uint(pinning, LIBBPF_PIN_BY_NAME);
} buffer SEC(".maps");

struct event {
  struct tcp_info ti;
  __u64 ts;
  __u64 cookie;
  __u16 	dport;
   __be32	daddr;
  __be32	saddr;
  __be16	sport;
};

struct bpf_reno {
	__u32	ack_cnt;	/* number of acks */
	__u32 	first_ssthresh;
	__u64 	first_packet_time;

};

SEC("struct_ops/tcp_reno_init")
void BPF_PROG(tcp_reno_init, struct sock *sk)
{
	struct bpf_reno *ca = inet_csk_ca(sk);
	ca->ack_cnt=0;
	ca->first_packet_time=0;
}

/* Or simply use the BPF_STRUCT_OPS to avoid the SEC boiler plate. */
SEC("struct_ops/tcp_reno_cong_avoid")
void BPF_PROG(tcp_reno_cong_avoid, struct sock *sk, __u32 ack, __u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct bpf_reno *ca = inet_csk_ca(sk);
	const struct  inet_sock *isk =  inet_sk(sk);

	if (ca->ack_cnt < 30) {
		struct event *event = bpf_ringbuf_reserve(&buffer, sizeof(struct event), 0);
		if (event){
			tcp_get_info(sk, &(event->ti));
			event->ts = bpf_ktime_get_ns();
			if (ca->first_packet_time == 0)
 				ca->first_packet_time = bpf_ktime_get_ns();
 			event->cookie = ca->first_packet_time;
			event->dport = isk->inet_dport;
			event->sport = isk->inet_sport;
			event->saddr = isk->inet_saddr;
			event->daddr = isk->inet_daddr;

			bpf_ringbuf_submit(event, 0);
			ca->ack_cnt++;
		}
    }
	
	// get tcp_info write to map, key is ips,ports

	if (!tcp_is_cwnd_limited(sk))
		return;

	/* In "safe" area, increase. */
	if (tcp_in_slow_start(tp)) {
		__u32 cwnd = min(tp->snd_cwnd + acked, tp->snd_ssthresh);

		acked -= cwnd - tp->snd_cwnd;
		tp->snd_cwnd = min(cwnd, tp->snd_cwnd_clamp);
		if (!acked)
			return;
	}
	/* In dangerous area, increase slowly. */
	if (tp->snd_cwnd_cnt >= tp->snd_cwnd) {
		tp->snd_cwnd_cnt = 0;
		tp->snd_cwnd++;
	}

	tp->snd_cwnd_cnt += acked;
	if (tp->snd_cwnd_cnt >= tp->snd_cwnd) {
		__u32 delta = tp->snd_cwnd_cnt / tp->snd_cwnd;

		tp->snd_cwnd_cnt -= delta * tp->snd_cwnd;
		tp->snd_cwnd += delta;
	}
	tp->snd_cwnd = min(tp->snd_cwnd, tp->snd_cwnd_clamp);
}

__u32 BPF_STRUCT_OPS(tcp_reno_ssthresh, struct sock *sk)
{
	const struct tcp_sock *tp = tcp_sk(sk);
	struct bpf_reno *ca = inet_csk_ca(sk);

	__u32 ssthresh = max(tp->snd_cwnd >> 1U, 2U);

	if (ca->first_ssthresh == 0) {
		
		const struct  inet_sock *isk =  inet_sk(sk);
		struct event *event = bpf_ringbuf_reserve(&buffer, sizeof(struct event), 0);
		if (event){
			tcp_get_info(sk, &(event->ti));
			event->ts = bpf_ktime_get_ns();
			event->cookie = ca->first_packet_time;
			event->dport = isk->inet_dport;
			event->sport = isk->inet_sport;
			event->saddr = isk->inet_saddr;
			event->daddr = isk->inet_daddr;
			event->ti.tcpi_snd_ssthresh = ssthresh;

			bpf_ringbuf_submit(event, 0);
			ca->first_ssthresh=ssthresh;
		}
    }
	return ssthresh;
}

__u32 BPF_STRUCT_OPS(tcp_reno_undo_cwnd, struct sock *sk)
{
	const struct tcp_sock *tp = tcp_sk(sk);
	return max(tp->snd_cwnd, tp->prior_cwnd);
}

SEC(".struct_ops")
struct tcp_congestion_ops reno = {
	.init		= (void *)tcp_reno_init,
	.ssthresh	= (void *)tcp_reno_ssthresh,
	.cong_avoid	= (void *)tcp_reno_cong_avoid,
	.undo_cwnd	= (void *)tcp_reno_undo_cwnd,
	.name		= "bpf_reno",
};
